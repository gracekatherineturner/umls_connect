import unittest
from connect_umls import UMLS
import environ
env = environ.Env()

# reading .env file to get api key
environ.Env.read_env()


class TestUMLS(unittest.TestCase):
    def test_unittest(self):
        umls = UMLS(env("UMLS_API_KEY"))

    def test_search_invalid(self):
        umls = UMLS(env("UMLS_API_KEY"))
        with self.assertRaises(Exception):
            results= umls.search("a", "BOO")
        with self.assertRaises(Exception):
            results= umls.search("a", "code")

    def test_search_valid(self):
        umls = UMLS(env("UMLS_API_KEY"))
        results = umls.search("hip pain joint")
        self.assertEqual(len(results), 46)
        self.assertEqual(results[0].cui, 'C0019559')
        self.assertEqual(results[0].name, 'Hip joint pain')
        synonyms = results[0].synonyms()
        if "HIP ARTHRALGIA" not in synonyms:
            self.assertEqual(False, True)
        self.assertEqual(results[0].hasMapping('SNOMEDCT_US'),True)
        self.assertEqual(results[0].hasMapping('RXNORM'),False)

    def test_search_k(self):
        umls = UMLS(env("UMLS_API_KEY"))
        results = umls.search("hip pain joint", 1)
    
        self.assertEqual(len(results), 1)
        results = umls.search("hip pain joint", 26)
        self.assertEqual(len(results), 26)
        results = umls.search("hip pain joint", None)
        self.assertEqual(len(results), 46)




if __name__ == '__main__':
    unittest.main()
